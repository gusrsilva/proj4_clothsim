#include <iostream>
#include <math.h>
#include <random>
#include <vector>

#include "cloth.h"
#include "collision/plane.h"
#include "collision/sphere.h"

using namespace std;

Cloth::Cloth(double width, double height, int num_width_points,
             int num_height_points, float thickness) {
    this->width = width;
    this->height = height;
    this->num_width_points = num_width_points;
    this->num_height_points = num_height_points;
    this->thickness = thickness;

    buildGrid();
    buildClothMesh();
}

Cloth::~Cloth() {
    point_masses.clear();
    springs.clear();

    if (clothMesh) {
        delete clothMesh;
    }
}

void Cloth::buildGrid() {
    // TODO (Part 1): Build a grid of masses and springs.

    // Create PointMasses
    double x(0), y(0), z(0);
    double x_step = width / (num_width_points - 1);
    double y_step = height / (num_height_points - 1);
    point_masses.reserve(num_width_points * num_height_points);
    for (int j = 0; j < num_height_points; j++) {
        if (orientation == HORIZONTAL) {
            y = 1;
            z = j * y_step;
        } else {
            y = j * y_step;
            double random = (1.0*rand())/RAND_MAX;
            z = (2.0*random - 1.0)/1000.0;
        }
        for (int i = 0; i < num_width_points; i++) {
            x = i * x_step;
            // Check if pinned
            bool isPinned = false;
            for (int k = 0; k < pinned.size(); k++) {
                if (i == pinned[k][0] and j == pinned[k][1]) {
                    isPinned = true;
                    break;
                }
            }
            Vector3D position = Vector3D(x, y, z);
            PointMass pm = PointMass(position, isPinned);
            point_masses.push_back(pm);
        }
    }

    // Create Springs
    for (int i = 0; i < num_width_points; i++) {
        for (int j = 0; j < num_height_points; j++) {
            //  Structural constraints exist between a point mass and the point mass to
            //      its left as well as the point mass above it.
            // Shearing constraints exist between a point mass and the point mass to
            //      its diagonal upper left as well as the point mass to its diagonal
            //      upper right.
            // Bending constraints exist between a point mass and the point mass two
            //      away to its right as well as the point mass two above it.

            PointMass *a = &point_masses[j * num_width_points + i];

            if (i - 1 >= 0) {    // Left pm exists
                PointMass *b = &point_masses[j * num_width_points + (i - 1)];
                springs.push_back(Spring(a, b, STRUCTURAL));
            }
            if (j - 1 >= 0) { // Top pm exists
                PointMass *b = &point_masses[(j - 1) * num_width_points + i];
                springs.push_back(Spring(a, b, STRUCTURAL));
            }
            if (i - 1 >= 0 and j - 1 >= 0) { // Upper Diagonal Left pm exists
                PointMass *b = &point_masses[(j - 1) * num_width_points + (i - 1)];
                springs.push_back(Spring(a, b, SHEARING));
            }
            if (i + 1 < num_width_points && j - 1 >= 0) { // Upper Diagonal Right pm exits
                PointMass *b = &point_masses[(j - 1) * num_width_points + (i + 1)];
                springs.push_back(Spring(a, b, SHEARING));
            }
            if (i + 2 < num_width_points) { // Two away to the right pm exists
                PointMass *b = &point_masses[j * num_width_points + (i + 2)];
                springs.push_back(Spring(a, b, BENDING));
            }
            if (j - 2 >= 0) { // Two above pm exists
                PointMass *b = &point_masses[(j - 2) * num_width_points + i];
                springs.push_back(Spring(a, b, BENDING));
            }
        }
    }


}

void Cloth::simulate(double frames_per_sec, double simulation_steps, ClothParameters *cp,
                     vector<Vector3D> external_accelerations,
                     vector<CollisionObject *> *collision_objects) {
    double mass = width * height * cp->density / num_width_points / num_height_points;
    double delta_t = 1.0f / frames_per_sec / simulation_steps;

    // TODO (Part 2): Compute total force acting on each point mass.
    Vector3D allAccs = Vector3D();
    for (Vector3D acc: external_accelerations) {
        allAccs += acc;
    }
//    allAccs /= external_accelerations.size();

    // Reset forces
    for (int i = 0; i < point_masses.size(); i++) {
        PointMass *pm = &point_masses[i];
        pm->forces = allAccs * mass;
    }

    for (int i = 0; i < springs.size(); i++) {
        Spring *s = &springs[i];
        if ((s->spring_type == SHEARING && !cp->enable_shearing_constraints)
            or (s->spring_type == BENDING && !cp->enable_bending_constraints)
            or (s->spring_type == STRUCTURAL && !cp->enable_structural_constraints)) {
            continue;
        }
        Vector3D diff = s->pm_b->position - s->pm_a->position;
        double fs = cp->ks * (diff.norm() - s->rest_length);
        Vector3D dir = diff.unit();
        Vector3D forceVector = dir * fs;

        s->pm_a->forces += forceVector;
        s->pm_b->forces -= forceVector;

    }

    // TODO (Part 2): Use Verlet integration to compute new point mass positions
    for (int i = 0; i < point_masses.size(); i++) {
        PointMass *pm = &point_masses[i];
        if (pm->pinned) {
            continue;
        }
        Vector3D xt_min_dt = pm->last_position;
        Vector3D xt = pm->position;
        double d = cp->damping / 100;
        double dt = delta_t;
        Vector3D at = pm->forces / mass;
        pm->position = xt + (1 - d) * (xt - xt_min_dt) + at * dt * dt;
        pm->last_position = xt;
    }

    // TODO (Part 4): Handle self-collisions.
    // This won't do anything until you complete Part 4.
    for (PointMass &pm : point_masses) {
        self_collide(pm, simulation_steps);
    }


    // TODO (Part 3): Handle collisions with other primitives.
    // This won't do anything until you complete Part 3.
    for (PointMass &pm : point_masses) {
        for (CollisionObject *co : *collision_objects) {
            co->collide(pm);
        }
    }


    // TODO (Part 2): Constrain the changes to be such that the spring does not change
    // in length more than 10% per timestep [Provot 1995].
    for (int i = 0; i < springs.size(); i++) {
        Spring *s = &springs[i];
        if (s->pm_a->pinned and s->pm_b->pinned) {
            continue;
        }
        Vector3D diffVec = s->pm_b->position - s->pm_a->position;
        double length = diffVec.norm();
        Vector3D direction = diffVec.unit();

        if (length > 1.1 * s->rest_length) {
            double diff = length - 1.1 * s->rest_length;
            // sub b, add a
            if (s->pm_a->pinned) {
                s->pm_b->position -= direction * diff;
            } else if (s->pm_b->pinned) {
                s->pm_a->position += direction * diff;
            } else {
                s->pm_a->position += 0.5 * direction * diff;
                s->pm_b->position -= 0.5 * direction * diff;
            }
        }
    }

}

void Cloth::build_spatial_map() {
    for (const auto &entry : map) {
        delete (entry.second);
    }
    map.clear();

    // TODO (Part 4): Build a spatial map out of all of the point masses.
    for(auto &pm: point_masses) {
        float hashVal = hash_position(pm.position);
        if(!map.count(hashVal)) {   // New Key
            map[hashVal] = new std::vector<PointMass *>();
        }
        map[hashVal]->push_back(&pm);
    }
}

void Cloth::self_collide(PointMass &pm, double simulation_steps) {
    // TODO (Part 4): Handle self-collision for a given point mass.
    float hashVal = hash_position(pm.position);
    if(map.count(hashVal)) {
        std::vector<PointMass *> *pointMasses = map[hashVal];

        Vector3D unscaledCorrection = Vector3D();
        int correctionsCount = 0;

        for (auto candidate_pm: *pointMasses) {
            if (pm.position == candidate_pm->position) {
                continue;
            }
            Vector3D diff = pm.position - candidate_pm->position;
            double distance = diff.norm();
            if (distance <= 2 * thickness) {
                Vector3D dir = diff.unit();
                double adjustDist = 2 * thickness - distance;
                unscaledCorrection += adjustDist * dir;
                correctionsCount++;
            }
        }
        if (correctionsCount > 0) {
            Vector3D scaledCorrections = (unscaledCorrection / correctionsCount) / simulation_steps;
            pm.position += scaledCorrections;
        }
    }
}

float Cloth::hash_position(Vector3D pos) {
    // TODO (Part 4): Hash a 3D position into a unique float identifier that represents
    // membership in some uniquely identified 3D box volume.
    double w = 3.0*width/num_width_points;
    double h = 3.0*height/num_height_points;
    double t = fmax(w,h);
    double a = 1.0, b = 1.0, c = 1.0;
    while((a+1)*w < pos.x) {a++;}
    while((b+1)*h < pos.y) {b++;}
    while((c+1)*t < pos.z) {c++;}

    return (float)(a + pow(b, 2.0) + pow(c, 3.0));
}

///////////////////////////////////////////////////////
/// YOU DO NOT NEED TO REFER TO ANY CODE BELOW THIS ///
///////////////////////////////////////////////////////

void Cloth::reset() {
    PointMass *pm = &point_masses[0];
    for (int i = 0; i < point_masses.size(); i++) {
        pm->position = pm->start_position;
        pm->last_position = pm->start_position;
        pm++;
    }
}

void Cloth::buildClothMesh() {
    if (point_masses.size() == 0) return;

    ClothMesh *clothMesh = new ClothMesh();
    vector<Triangle *> triangles;

    // Create vector of triangles
    for (int y = 0; y < num_height_points - 1; y++) {
        for (int x = 0; x < num_width_points - 1; x++) {
            PointMass *pm = &point_masses[y * num_width_points + x];
            // Both triangles defined by vertices in counter-clockwise orientation
            triangles.push_back(new Triangle(pm, pm + num_width_points, pm + 1));
            triangles.push_back(new Triangle(pm + 1, pm + num_width_points,
                                             pm + num_width_points + 1));
        }
    }

    // For each triangle in row-order, create 3 edges and 3 internal halfedges
    for (int i = 0; i < triangles.size(); i++) {
        Triangle *t = triangles[i];

        // Allocate new halfedges on heap
        Halfedge *h1 = new Halfedge();
        Halfedge *h2 = new Halfedge();
        Halfedge *h3 = new Halfedge();

        // Allocate new edges on heap
        Edge *e1 = new Edge();
        Edge *e2 = new Edge();
        Edge *e3 = new Edge();

        // Assign a halfedge pointer to the triangle
        t->halfedge = h1;

        // Assign halfedge pointers to point masses
        t->pm1->halfedge = h1;
        t->pm2->halfedge = h2;
        t->pm3->halfedge = h3;

        // Update all halfedge pointers
        h1->edge = e1;
        h1->next = h2;
        h1->pm = t->pm1;
        h1->triangle = t;

        h2->edge = e2;
        h2->next = h3;
        h2->pm = t->pm2;
        h2->triangle = t;

        h3->edge = e3;
        h3->next = h1;
        h3->pm = t->pm3;
        h3->triangle = t;
    }

    // Go back through the cloth mesh and link triangles together using halfedge
    // twin pointers

    // Convenient variables for math
    int num_height_tris = (num_height_points - 1) * 2;
    int num_width_tris = (num_width_points - 1) * 2;

    bool topLeft = true;
    for (int i = 0; i < triangles.size(); i++) {
        Triangle *t = triangles[i];

        if (topLeft) {
            // Get left triangle, if it exists
            if (i % num_width_tris != 0) { // Not a left-most triangle
                Triangle *temp = triangles[i - 1];
                t->pm1->halfedge->twin = temp->pm3->halfedge;
            } else {
                t->pm1->halfedge->twin = nullptr;
            }

            // Get triangle above, if it exists
            if (i >= num_width_tris) { // Not a top-most triangle
                Triangle *temp = triangles[i - num_width_tris + 1];
                t->pm3->halfedge->twin = temp->pm2->halfedge;
            } else {
                t->pm3->halfedge->twin = nullptr;
            }

            // Get triangle to bottom right; guaranteed to exist
            Triangle *temp = triangles[i + 1];
            t->pm2->halfedge->twin = temp->pm1->halfedge;
        } else {
            // Get right triangle, if it exists
            if (i % num_width_tris != num_width_tris - 1) { // Not a right-most triangle
                Triangle *temp = triangles[i + 1];
                t->pm3->halfedge->twin = temp->pm1->halfedge;
            } else {
                t->pm3->halfedge->twin = nullptr;
            }

            // Get triangle below, if it exists
            if (i + num_width_tris - 1 < 1.0f * num_width_tris * num_height_tris / 2.0f) { // Not a bottom-most triangle
                Triangle *temp = triangles[i + num_width_tris - 1];
                t->pm2->halfedge->twin = temp->pm3->halfedge;
            } else {
                t->pm2->halfedge->twin = nullptr;
            }

            // Get triangle to top left; guaranteed to exist
            Triangle *temp = triangles[i - 1];
            t->pm1->halfedge->twin = temp->pm2->halfedge;
        }

        topLeft = !topLeft;
    }

    clothMesh->triangles = triangles;
    this->clothMesh = clothMesh;
}
