#include <nanogui/nanogui.h>

#include "../clothMesh.h"
#include "../misc/sphere_drawing.h"
#include "sphere.h"

using namespace nanogui;
using namespace CGL;

void Sphere::collide(PointMass &pm) {
    // TODO (Part 3): Handle collisions with spheres.
    Vector3D pos = pm.position;
    double distToOrigin = (origin-pos).norm();
    if(distToOrigin >= radius) {
        return;
    }

    Vector3D dirOriginToPos = (pm.last_position-origin).unit();
    Vector3D tangentPoint = origin + radius*dirOriginToPos;
    Vector3D correctionVector = tangentPoint - pm.last_position;

    pm.position = pm.last_position + correctionVector*(1-friction);
}

void Sphere::render(GLShader &shader) {
    // We decrease the radius here so flat triangles don't behave strangely
    // and intersect with the sphere when rendered
    Misc::draw_sphere(shader, origin, radius * 0.92);
}
