#include "iostream"
#include <nanogui/nanogui.h>

#include "../clothMesh.h"
#include "../clothSimulator.h"
#include "plane.h"

using namespace std;
using namespace CGL;

#define SURFACE_OFFSET 0.0001 // 0.0001

void Plane::collide(PointMass &pm) {
//    Vector3D point = this->point + SURFACE_OFFSET*normal;
    double hitLastPos = dot(point - pm.last_position, normal);
    double hitCurrPos = dot(point-pm.position, normal);

    if(hitLastPos >= 0 && hitCurrPos <= 0) { // Normal is same direction
        double hitT = dot(point - pm.position, normal)/dot(normal, normal);
        std::cout << " Normal Same Direction. hitT: " << hitT << std::endl;
        Vector3D tangentPoint = pm.position - hitT*normal;
        Vector3D correctionVector = tangentPoint - pm.last_position;
        correctionVector += normal*SURFACE_OFFSET;
        pm.position = pm.last_position + correctionVector*(1.0-friction);
    }
    else if(hitLastPos <= 0 && hitCurrPos >= 0) { // Normal is opposite direction
        double hitT = dot(point - pm.position, normal)/dot(normal, normal);
        Vector3D tangentPoint = pm.position + hitT*normal;
        Vector3D correctionVector = tangentPoint - pm.last_position;
        correctionVector += normal*SURFACE_OFFSET;
        pm.position = pm.last_position + correctionVector*(1.0-friction);
    }
}

void Plane::render(GLShader &shader) {
    nanogui::Color color(0.7f, 0.7f, 0.7f, 1.0f);

    Vector3f sPoint(point.x, point.y, point.z);
    Vector3f sNormal(normal.x, normal.y, normal.z);
    Vector3f sParallel(normal.y - normal.z, normal.z - normal.x,
                       normal.x - normal.y);
    sParallel.normalize();
    Vector3f sCross = sNormal.cross(sParallel);

    MatrixXf positions(3, 4);
    MatrixXf normals(3, 4);

    positions.col(0) << sPoint + 2 * (sCross + sParallel);
    positions.col(1) << sPoint + 2 * (sCross - sParallel);
    positions.col(2) << sPoint + 2 * (-sCross + sParallel);
    positions.col(3) << sPoint + 2 * (-sCross - sParallel);

    normals.col(0) << sNormal;
    normals.col(1) << sNormal;
    normals.col(2) << sNormal;
    normals.col(3) << sNormal;

    if (shader.uniform("in_color", false) != -1) {
        shader.setUniform("in_color", color);
    }
    shader.uploadAttrib("in_position", positions);
    shader.uploadAttrib("in_normal", normals);

    shader.drawArray(GL_TRIANGLE_STRIP, 0, 4);
}
